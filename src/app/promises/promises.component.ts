import { Component, OnInit } from "@angular/core";

@Component({
  selector: "app-promises",
  templateUrl: "./promises.component.html",
  styleUrls: ["./promises.component.css"],
})
export class PromisesComponent implements OnInit {
  constructor() {}

  ngOnInit() {}
  ////////////////////with callbacks
  // trackUserHandler() {
  //   navigator.geolocation.getCurrentPosition(
  //     (posData) => {
  //       setTimeout(() => {
  //         console.log(posData);
  //       }, 2000);
  //     },
  //     (error) => {
  //       console.log(error);
  //     }
  //   );
  //   setTimeout(() => {
  //     console.log("Timer done!");
  //   }, 0);
  //   console.log("Getting position...");
  // }

  /////////////////////////////////with promises
  trackUserHandler() {
    let positionData;
    this.getPosition()
      .then((posData) => {
        positionData = posData;
        return this.setTimer(2000);
      })
      .then((data) => {
        console.log(data, positionData);
      })
      .catch((err) => {
        console.log(err);
      });
    this.setTimer(1000).then(() => {
      console.log("Timer done!");
    });
    console.log("Getting position...");
  }

  getPosition = () => {
    const promise = new Promise((resolve, reject) => {
      navigator.geolocation.getCurrentPosition(
        (success) => {
          resolve(success);
        },
        (error) => {
          reject(error);
        }
      );
    });
    return promise;
  };

  setTimer = (duration) => {
    const promise = new Promise((resolve, reject) => {
      setTimeout(() => {
        resolve("Done!");
      }, duration);
    });
    return promise;
  };
  ////////////////////////////////////////////////

  // shorterFilterArray() {
  //   const numbers = [3, 4, 10, 20];
  //   const lesserThanFive = numbers.filter((num) => num < 5);
  //   console.log(lesserThanFive); //[3,4]
  // }

  // filterArray() {
  //   const numbers = [3, 4, 10, 20];

  //   // Passing getLessThanFive function into filter
  //   const lesserThanFive = numbers.filter(this.getLessThanFive);
  //   console.log(lesserThanFive); //[3,4]
  // }

  // getLessThanFive = (num) => num < 5;
  /////////////////////////////////////////////////////////

  // makeBurgerWithCallbacks() {
  //   //   1. Get beef
  //   //   2. Cook the beef
  //   //   3. Get buns for the burger
  //   //   4. Put the cooked beef between the buns
  //   //   5. Serve the burger (from the callback)
  //   const makeBurger = (nextStep) => {
  //     this.getBeef((beef) => {
  //       this.cookBeef(beef, (cookedBeef) => {
  //         this.getBuns((buns) => {
  //           this.putBeefBetweenBuns(buns, beef, (burger) => {
  //             nextStep(burger);
  //           });
  //         });
  //       });
  //     });
  //   };
  // }

  // // Make and serve the burger

  // getBeef() {
  //   const prom = new Promise((resolve, reject) => {
  //     setTimeout(() => {
  //       console.log("Get beef");
  //       resolve();
  //     }, 1000);
  //   });

  //   return prom;
  // }

  // cookBeef() {
  //   console.log("Beef cooked");
  // }

  // getBuns(buns) {
  //   console.log("Got Buns", buns);
  // }

  // putBeefBetweenBuns(buns) {
  //   console.log(buns, beef, callback);
  // }
  // serve(e) {}

  // makeBurgerWithPromis() {
  //   const makeBurger = () => {
  //     return this.getBeef()
  //       .then((beef) => this.cookBeef())
  //       .then((cookedBeef) => this.getBuns(cookedBeef))
  //       .then((bunsAndBeef) => this.putBeefBetweenBuns(bunsAndBeef));
  //   };

  //   // Make and serve burger
  //   makeBurger().then((burger) => this.serve(burger));
  // }

  // ////////////////////////////////////
  // numbers = [3, 4, 10, 20];
  // getLessThanFive = (num) => num < 5;

  // // Passing getLessThanFive function into filter
  // lesserThanFive = this.numbers.filter(this.getLessThanFive);
}
