import { Component, OnDestroy, OnInit } from "@angular/core";
import { Subscription, Observable, timer, Subject } from "rxjs";
import { takeUntil } from "rxjs/operators";

@Component({
  template: ` <p>Page 1</p> `,
})
export class HomePageComponent implements OnInit, OnDestroy {
  // everySecond$: Observable<number> = timer(0, 1000);
  everyTwoSeconds$: Observable<number> = timer(0, 2000);
  //   case 1: simple .unsubscribe()
  //   secondsSubscription: Subscription;

  //   ngOnInit() {
  //     this.secondsSubscription = this.everySecond$.subscribe((second) => {
  //       const randomNumber = Math.floor(Math.random() * 100);
  //       console.log(randomNumber);
  //     });
  //   }

  //   ngOnDestroy() {
  //     this.secondsSubscription.unsubscribe();
  //   }

  //case 2 collect subscriptions with subscription.add()
  //   secondsSubscription: Subscription = new Subscription();
  //   everySecond$: Observable<number> = timer(0, 1000);

  //   ngOnInit() {
  //     this.secondsSubscription.add(
  //       this.everySecond$.subscribe(() => {
  //         const randomNumber = Math.floor(Math.random() * 100);
  //         console.log("one second", randomNumber);
  //       })
  //     );

  //     this.secondsSubscription.add(
  //       this.everyTwoSecond$.subscribe(() => {
  //         const randomNumber = Math.floor(Math.random() * 100);
  //         console.log("two seconds", randomNumber);
  //       })
  //     );
  //   }

  //   ngOnDestroy() {
  //     this.secondsSubscription.unsubscribe();
  //   }

  //case3 takeUntil from rxjs
  everySecond$: Observable<number> = timer(0, 1000);
  unsubscribeSubject = new Subject();

  ngOnInit() {
    this.everySecond$.pipe(takeUntil(this.unsubscribeSubject)).subscribe(() => {
      const randomNumber = Math.floor(Math.random() * 100);
      console.log(randomNumber);
    });
  }

  ngOnDestroy() {
    this.unsubscribeSubject.next();
    this.unsubscribeSubject.complete();
  }
}
