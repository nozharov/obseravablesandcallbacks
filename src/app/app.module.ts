import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";

import { AppComponent } from "./app.component";
import { RouterModule } from "@angular/router";
import { HomePageComponent } from "./home-page/home-page.component";
import { ReallyComponent } from "./other-page/other-page.component";
import { HomePageService } from "./home-page/home-page.service";
import { PromisesComponent } from "./promises/promises.component";

@NgModule({
  declarations: [
    AppComponent,
    HomePageComponent,
    ReallyComponent,
    PromisesComponent,
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot([
      { path: "", redirectTo: "home", pathMatch: "full" },
      { path: "home", component: HomePageComponent, pathMatch: "full" },
      { path: "other", component: ReallyComponent, pathMatch: "full" },
      { path: "promises", component: PromisesComponent, pathMatch: "full" },
    ]),
  ],
  providers: [HomePageService],
  bootstrap: [AppComponent],
})
export class AppModule {}
